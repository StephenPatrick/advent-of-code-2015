package main

import "fmt"
import "strings"
import "strconv"
import "time"
import "bytes"

func g_atoi(s string) int{
    i, _ := strconv.Atoi(s)
    return i
}

func runeToStr(r rune) string{
    return fmt.Sprintf("%c",r)
}

func main() {
    var buffer bytes.Buffer
    digits := "11131221130"
    prevChar := '0'
    charRepeat := 1
    nextDigits := ""
    start := time.Now()
    for i := 0; i < 50; i++ {
        for _,c := range digits {
            if (prevChar != '0') {
                if (c != prevChar) {
                    buffer.WriteString(strconv.Itoa(charRepeat))
                    buffer.WriteRune(prevChar)
                    charRepeat = 1
                } else {
                    charRepeat++
                }
            }
            prevChar = c
        }
        fmt.Println(i)
        nextDigits = buffer.String()
        fmt.Println(len(nextDigits))
        prevChar = '0'
        charRepeat = 1
        digits = strings.Join([]string{nextDigits,"0"},"")
        buffer.Reset()
        fmt.Println(time.Since(start))
        fmt.Println("")
        start = time.Now()
    }
}
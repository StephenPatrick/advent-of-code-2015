package main

import "fmt"
import "strings"
import "strconv"
import "time"

func g_atoi(s string) int{
    i, _ := strconv.Atoi(s)
    return i
}

func runeToStr(r rune) string{
    return fmt.Sprintf("%c",r)
}

func main() {
    digits := "11131221130"
    prevChar := '0'
    charRepeat := 1
    nextDigits := ""
    start := time.Now()
    for i := 0; i < 40; i++ {
        for _,c := range digits {
            if (prevChar != 0) {
                if (c != prevChar) {
                    nextDigits = nextDigits + strconv.Itoa(charRepeat) + runeToStr(prevChar)
                    charRepeat = 1
                } else {
                    charRepeat += 1
                }
            }
            prevChar = c
        }
        fmt.Println(len(nextDigits))
        prevChar = '0'
        charRepeat = 1
        digits = strings.Join([]string{nextDigits,"0"},"")
        nextDigits = ""
        fmt.Println(time.Since(start))
        fmt.Println("")
        start = time.Now()
    }
}
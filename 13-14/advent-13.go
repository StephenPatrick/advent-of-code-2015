package main

import "bufio"
import "os"
import "fmt"
import "strings"
import "regexp"
import "strconv"

func isOpcode(s string) bool {
    return (s == "NOT" || s == "AND" ||
                s == "OR" || s == "LSHIFT" ||
                s == "RSHIFT") 
}

func isInt(s string) bool {
    comp := regexp.MustCompile(`^[\d\-]*$`)
    return comp.MatchString(s)
}

func g_atoi(s string) int{
    i, _ := strconv.Atoi(s)
    return i
}

// Didn't write this
func singleContains(s []string, e string) int {
    for i, a := range s {
        if a == e {
            return i
        }
    }
    return -1
}

func main() {
    
    file, _ := os.Open("advent-13-input.txt") // For read access.
    
    scanner := bufio.NewScanner(file)
    results := map[string][]string{}
    propagated := map[string]bool{}
    
    // Process input
    for scanner.Scan() {
        parts := strings.Fields(scanner.Text())
        divider := 0
        for i := range parts {
            if (parts[i] == "->") {
                divider= i;
            }
        }
        results[parts[len(parts)-1]] = parts[0:divider]
    }
    // Propagate all values until a is defined
    for !propagated["a"] {
        for key, value := range results {
            if propagated[key] {
                continue
            }
            // When there's a single element, it's a number,
            // propagate that value.
            if len(value) == 1 && isInt(value[0]) {
                for key2, value2 := range results {
                    if key == key2  {
                        continue
                    } else {
                        index := singleContains(value2,key)
                        if index != -1 {
                            results[key2][index] = value[0]
                        }
                    }
                }
                propagated[key] = true
            // Not
            } else if isOpcode(value[0]) && isInt(value[1]) {
                results[key] = []string{strconv.Itoa(^g_atoi(value[1]))}
            // Binary Operators
            } else if isInt(value[0]) && isOpcode(value[1]) && isInt(value[2]) {
                arg1 := g_atoi(value[0])
                arg2 := g_atoi(value[2])
                opcode := value[1]
                if (opcode == "RSHIFT") {
                    results[key] = []string{strconv.Itoa(arg1 >> uint(arg2))}
                } else if (opcode == "LSHIFT") {
                    results[key] = []string{strconv.Itoa(arg1 << uint(arg2))}
                } else if (opcode == "AND") {
                    results[key] = []string{strconv.Itoa(arg1 & arg2)}
                } else if (opcode == "OR") {
                    results[key] = []string{strconv.Itoa(arg1 | arg2)}
                }
            }
        }
    }
    fmt.Println(results["a"])
}
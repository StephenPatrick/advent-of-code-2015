#load "str.cma";;
open Printf;;

let file = "advent-11-input.txt";;
let lights = Array.make_matrix 1000 1000 0;;

let read_toggle p1 p2 =
    printf "%s, %s\n" p1 p2;
    ;;
    
let read_on p1 p2 =
    printf "%s, %s\n" p1 p2;
    let points1 = Str.split (Str.regexp ",") p1;
    let points2 = Str.split (Str.regexp ",") p2;
    for i = (List.nth points1 0) to (List.nth points2 0) do
        for j = (List.nth points1 1) to (List.nth points2 1) do
            Array.set lights.(i) j 1;
        done;
    done;
    ;;
    
let read_off p1 p2 =
    printf "%s, %s\n" p1 p2;
    ;;

let read_instruction str_array =
    if List.nth str_array 0 = "toggle" then read_toggle (List.nth str_array 1) (List.nth str_array 3)
    else 
        if List.nth str_array 1 = "on" then read_on (List.nth str_array 2) (List.nth str_array 4)
        else read_off (List.nth str_array 2) (List.nth str_array 4)
    ;;

let main () =
    let ic = open_in file in
    for i = 0 to 299 do
        read_instruction (Str.split (Str.regexp " +") (input_line ic))
    done;
    flush stdout;
    close_in ic
    ;;
        
main();;
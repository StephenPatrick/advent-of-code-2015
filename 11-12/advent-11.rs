use std::io::{BufReader,BufRead}; 
use std::fs::File;

fn main() {
    
    
    let mut lights = [[0u8; 1000]; 1000];
   /* 
    for i in  0..lights.len() {
        for j in  0..lights[i].len() {
            lights[i][j] = 0;
        }
    }*/
    
    let file = BufReader::new((File::open("advent-11-input.txt")).unwrap());
    
    let mut set: u8 = 1;
    let mut pair1ind = 2;
    let mut pair2ind = 4;
    
    for newline in file.lines() {
        let line = newline.unwrap();
        let parts = line.split(" ").collect::<Vec<&str>>();
        set = 1;
        pair1ind = 2;
        pair2ind = 4;
        
        
        if parts[0] == "toggle" {
            set = 2;
            pair1ind = 1;
            pair2ind = 3;
        } else if parts[1] == "off" {
            set = 0;
        } 
        
        let firstPairs = parts[pair1ind].split(",").collect::<Vec<&str>>();
        let secondPairs = parts[pair2ind].split(",").collect::<Vec<&str>>();
        
        for i in firstPairs[0].parse::<usize>().unwrap()..secondPairs[0].parse::<usize>().unwrap()+1 {
            for j in firstPairs[1].parse::<usize>().unwrap()..secondPairs[1].parse::<usize>().unwrap()+1 {
                if set != 2 {
                    lights[i][j] = set;
                } else {
                    lights[i][j] = (lights[i][j] + 1) % 2;
                }
            }
        }
    }
    
    let mut sum: u64 = 0;
    
    for i in  0..lights.len() {
        for j in  0..lights[i].len() {
            sum += lights[i][j] as u64;
        }
    }
    
    println!("{0}",sum);
}


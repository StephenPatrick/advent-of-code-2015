package main

import "bufio"
import "os"
import "fmt"
import "log"
import "strings"
import "strconv"
import "math"

func g_atoi(s string) int{
    i, _ := strconv.Atoi(s)
    return i
}

func main() {
    
    lights := [1000][1000]int{}
    
    for i := range lights {
        for j := range lights[i] {
            lights[i][j] = 0
        }
    }
    
    file, err := os.Open("advent-11-input.txt") // For read access.
    if err != nil {
        log.Fatal(err)
    }
    
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        parts := strings.Fields(scanner.Text())
        fmt.Println(parts, len(parts))
        set := 1
        pair1ind := 2
        pair2ind := 4
        if (parts[0] == "toggle") {
            set = 2
            pair1ind = 1
            pair2ind = 3
        } else if (parts[1] == "off") {
            set = 0
        } 
        firstPairs := strings.Split(parts[pair1ind],",")
        secondPairs := strings.Split(parts[pair2ind],",")
        for i := g_atoi(firstPairs[0]); i <= g_atoi(secondPairs[0]); i++ {
            for j := g_atoi(firstPairs[1]); j <= g_atoi(secondPairs[1]); j++ {
                if (set == 0) {
                    lights[i][j] = int(math.Max(0,float64(lights[i][j]-1))) 
                } else {
                    lights[i][j] += set
                }
            }
        }
    }
    
    sum := 0
    
    for i := range lights {
        for j := range lights[i] {
            sum += lights[i][j]
        }
    }
    
    fmt.Println(sum)
}
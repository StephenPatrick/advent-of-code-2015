def main():
  floor = 0
  position = 0
  parens = open('advent-1-input.txt', 'r')
  while True:
    c = parens.read(1)
    if not c:
      break
    if c == '(':
      floor += 1
    elif c == ')':
      floor -= 1
    position += 1
    if floor == -1:
      break
  #end loop
  print(position)
#end main


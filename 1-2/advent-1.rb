def main()
    floor = 0
    parens = open('advent-1-input.txt', 'r')
    parens.each_char do |c|
        if c == '('
            floor += 1
        elsif c == ')'
            floor -= 1
        end
    end
    puts floor
end

main()
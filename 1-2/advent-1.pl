use warnings;
use strict;

sub main{
  my $floor = 0;
  open my $parens, '<', 'advent-1-input.txt';
  while ((my $n = read ($parens, my $char, 1)) != 0) {
    if (ord($char) == 40){ #Perl does not want to check equality against paren chars.
      $floor++;
    }
    elsif (ord($char) == 41){
      $floor--;
    }
  }
  print($floor);
}

main();
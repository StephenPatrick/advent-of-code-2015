def main()
    floor = 0
    position = 1
    parens = open('advent-1-input.txt', 'r')
    parens.each_char do |c|
        if c == '('
            floor += 1
        elsif c == ')'
            floor -= 1
        end
        if floor == -1
            break
        end
        position += 1
    end
    puts position
end

main()
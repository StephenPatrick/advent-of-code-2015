function main()
    floor = 0
    position = 0
    parens = assert(io.open('advent-1-input.txt', 'r'))
    io.input(parens)
    while true do
        c = io.read(1)
        if c == nil then
            break
        elseif c == '(' then
            floor = floor + 1
        elseif c == ')' then
            floor = floor - 1
        end
        position = position + 1
        if floor == -1 then
            break;
        end
    end
    print(position)
end

main()
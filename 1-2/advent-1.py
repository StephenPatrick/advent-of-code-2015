# Problem 1
def main():
    floor = 0
    parens = open('advent-1-input.txt', 'r')
    while True:
        c = parens.read(1)
        if not c:
            break
        if c == '(':
            floor += 1
        elif c == ')':
            floor -= 1
    #end loop
print(floor)
#end main

if __name__ == "__main__":
    main()
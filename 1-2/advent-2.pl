use warnings;
use strict;

sub main{
  my $floor = 0;
  my $position = 0;
  open my $parens, '<', 'advent-1-input.txt';
  while ((my $n = read ($parens, my $char, 1)) != 0) {
    if (ord($char) == 40){
      $floor++;
    }
    elsif (ord($char) == 41){
      $floor--;
    }
    $position++;
    if ($floor == -1){
      last;
    }
  }
  print($position);
}

main();
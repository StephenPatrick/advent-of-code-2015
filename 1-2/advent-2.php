<?php
    $floor = 0;
    $position = 0;
    $file = fopen("advent-1-input.txt","r");
    while (($c = fgetc($file)) !== false) {
        if ($c == '(') {
            $floor++;
        } 
        elseif ($c == ')') {
            $floor--;
        }
        $position++;
        if ($floor == -1){
            break;
        }
    }
    print($position);
?>
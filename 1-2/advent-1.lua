function main()
    floor = 0
    parens = assert(io.open('advent-1-input.txt', 'r'))
    io.input(parens)
    while true do
        c = io.read(1)
        if c == nil then
            break
        elseif c == '(' then
            floor = floor + 1
        elseif c == ')' then
            floor = floor - 1
        end
    end
    print(floor)
end

main()
<?php
    $floor = 0;
    $file = fopen("advent-1-input.txt","r");
    while (($c = fgetc($file)) !== false) {
        if ($c == '(') {
            $floor++;
        } 
        elseif ($c == ')') {
            $floor--;
        }
    }
    print($floor);
?>
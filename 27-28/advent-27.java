import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Scanner;
import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;

class Advent27 {

    public static void main(String[] args) throws FileNotFoundException{

        // Load file
        Scanner content = new Scanner(new File("advent-27-input.txt")).useDelimiter("\\Z");
        HashMap<String,ArrayList<String>> reindeerMap = new HashMap<>();
        while (content.hasNext()){
            String[] name = content.nextLine().split(",",4);
            ArrayList<String> list = new ArrayList<>();
            list.add(name[1]);
            list.add(name[2]);
            list.add(name[3]);
            reindeerMap.put(name[0],list);
        }
        int seconds = 2503;
        int max_distance = 0;
        for (ArrayList<String> reindeerStats: reindeerMap.values()) {
            int countDown = seconds;
            int kmh = Integer.parseInt(reindeerStats.get(0));
            int stamina = Integer.parseInt(reindeerStats.get(1));
            int delay = Integer.parseInt(reindeerStats.get(2));
            int distance = 0;
            while (countDown > 0) {
                // We go this far in stamina seconds
                distance += kmh * stamina;
                countDown -= stamina;
                // Unless this brings us below 0
                if (countDown < 0) {
                    // In which case we actually didn't go that far
                    distance -= kmh * countDown * -1;
                }
                // Then we are tired and stop.
                // If this brings us below 0, this isn't a problem.
                countDown -= delay;
            }
            max_distance = (distance > max_distance) ? distance : max_distance;
        }
        System.out.println(max_distance);
        
    }
}

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;

class Advent28 {
    
    static class Reindeer {
        
        public int kmh;
        public int stamina;
        public int delay;
        public int distance;
        public int incrementor;
        public int points;
        
        public Reindeer(int kmh, int stamina, int delay){
            this.kmh = kmh;
            this.stamina = stamina;
            this.delay = delay;
            this.distance = 0;
            this.incrementor = 0;
            this.points = 0;
        } 
        
    }

    public static void main(String[] args) throws FileNotFoundException{

        // Load file
        Scanner content = new Scanner(new File("advent-27-input.txt")).useDelimiter("\\Z");
        ArrayList<Reindeer> reindeerMap = new ArrayList<>();
        while (content.hasNext()){
            // The input was trimmed to csv format of relevant data (name, ints).
            String[] name = content.nextLine().split(",",4);
            reindeerMap.add(new Reindeer(Integer.parseInt(name[1]),
                                         Integer.parseInt(name[2]),
                                         Integer.parseInt(name[3])));
        }
        int max_points = 0;
        for (int seconds = 2503; seconds > 0; seconds--){
            int max_distance = 0;
            for (Reindeer reindeer: reindeerMap) {
                if (reindeer.incrementor < reindeer.stamina) {
                    reindeer.distance += reindeer.kmh; 
                }
                if (reindeer.distance > max_distance) {
                    max_distance = reindeer.distance;
                }
                System.out.println(reindeer.points + " " +reindeer.incrementor);
                reindeer.incrementor = (reindeer.incrementor + 1) % (reindeer.stamina+reindeer.delay);
            }
            // All tied reindeer get a point
            for (Reindeer reindeer: reindeerMap) {
                if (reindeer.distance == max_distance) {
                    reindeer.points++;
                }
            }
            System.out.println("");
        }
    }
}

import java.util.Scanner;
import java.util.ArrayList;
import java.io.File;
import java.io.FileNotFoundException;

class Advent25 {
    
    static class Permutations {
        
        public ArrayList<String> value;
        
        public Permutations(String input){
            this.value = new ArrayList<>();
            permute("",input);
        }
        
        
        private void permute(String prefix, String str){
            int n = str.length();
            if (n == 0) 
                this.value.add(prefix);
            else {
                for (int i = 0; i < n; i++)
                    permute(prefix + str.charAt(i), 
                str.substring(0, i) + str.substring(i+1));
            }
        }   
        
    }
    
    public static void main(String[] args) throws FileNotFoundException{

        // Load file
        Scanner content = new Scanner(new File("advent-25-input.txt")).useDelimiter("\\Z");
        int[][] values = new int[8][8];
        for (int i = 0; i < values.length; i++) {
            for (int k = 0; k < values[i].length; k++) {
                values[i][k] = 0;
            }
        }
        
        while (content.hasNext()){
            // The input was trimmed to csv format of relevant data (name, ints).
            String[] input = content.nextLine().split(",",3);
            values[nameToIndex(input[0])][nameToIndex(input[2])] += Integer.parseInt(input[1]);
            values[nameToIndex(input[2])][nameToIndex(input[0])] += Integer.parseInt(input[1]);
        }
        for (int i = 0; i < values.length; i++) {
            for (int k = 0; k < values[i].length; k++) {
                System.out.println(values[i][k]);
            }
        }
        
        Permutations perms = new Permutations("01234567");
        ArrayList<String> s_perms = perms.value;
        
        int max_val = 0;
        for (String s_order: s_perms) {
            int[] order = stringToIntray(s_order);
            System.out.println(s_order);
            int val = values[order[7]][order[0]];
            for (int i = 0; i < order.length-1; i++) {
                val += values[order[i]][order[i+1]];
            }
            max_val = (val > max_val) ? val : max_val;
        }
        System.out.println(max_val);
    }
    
    public static int[] stringToIntray(String s_order){
        int[] order = new int[8];
        for (int i = 0, n = s_order.length(); i < n; i++) {
            order[i] = (int)s_order.charAt(i)-48;
        }
        return order;
        
    }
    
    public static int nameToIndex(String name) {
        switch (name){
            case "Alice": return 0;
            case "Bob": return 1;
            case "Carol": return 2;
            case "David": return 3;
            case "Eric": return 4;
            case "Frank": return 5;
            case "George": return 6;
            case "Mallory": return 7;
        }
        return 0;
    }
    
    
 }

package com.company;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.File;
import java.io.IOException;


class Main {

    public static void main(String[] args) throws IOException{

        // Load file
        //String content = new Scanner(new File("advent-23-input.txt")).useDelimiter("\\Z").next();
        JsonFactory factory = new JsonFactory();
        JsonParser parser = factory.createJsonParser(new File("advent-23-input.txt"));

        int sum = 0;
        parser.nextToken();
        sum += array_find_ints(parser);
        System.out.println(sum);
    }

    public static int array_find_ints(JsonParser parser) throws IOException {
        int sum = 0;
        while (true) {
            JsonToken token = parser.nextToken();
            System.out.println(parser.getText());
            // Recursive case
            if (token == JsonToken.START_OBJECT) {
                sum += obj_find_ints(parser);
            } else if (token == JsonToken.START_ARRAY) {
                sum += array_find_ints(parser);
            // End case
            } else if (token == JsonToken.END_ARRAY) {
                return sum;
            // Sum all ints
            } else if (token == JsonToken.VALUE_NUMBER_INT) {
                sum += parser.getIntValue();
            }
        }
    }

    public static int obj_find_ints(JsonParser parser) throws IOException {
        int sum = 0;
        while (true) {
            JsonToken token = parser.nextToken();
            System.out.println(parser.getText());
            // Recursive case
            if (token == JsonToken.START_OBJECT) {
                sum += obj_find_ints(parser);
            } else if (token == JsonToken.START_ARRAY) {
                sum += array_find_ints(parser);
                // End case
            } else if (token == JsonToken.END_OBJECT) {
                return sum;
            } else if (token == JsonToken.VALUE_NUMBER_INT) {
                sum += parser.getIntValue();
            }
            else if (token == JsonToken.VALUE_STRING) {
                if (parser.getText().equals("red")) {
                    // Keep going forward
                    // Ignore whatever is returned
                    obj_find_ints(parser);
                    System.out.println("--SKIPPED");
                    return 0;
                }
            }
        }
    }
}
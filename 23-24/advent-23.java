import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

class Advent23 {

    public static void main(String[] args) throws FileNotFoundException{

        // Load file
        String content = new Scanner(new File("advent-23-input.txt")).useDelimiter("\\Z").next();
        
        Pattern numbers = Pattern.compile("[^[\\d|-]]*([\\d|-]+)");
        Matcher number_finder = numbers.matcher(content);
        
        int sum = 0;
        
        while (number_finder.find()) {
            sum += Integer.parseInt(number_finder.group());
        }        
        
        System.out.println(sum);
    }
}
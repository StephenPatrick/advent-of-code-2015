package main

import "fmt"
 
func main() {
    salesman(8)
}

func salesman(n int) {
    
    distances := [8][8]int{}
    distances[0] = [8]int{0,129,58,13,24,60,71,67}
    distances[1] = [8]int{129,0,142,15,135,75,82,54}
    distances[2] = [8]int{58,142,0,118,122,103,49,97}
    distances[3] = [8]int{13,15,118,0,116,12,18,91}
    distances[4] = [8]int{24,135,122,116,0,129,53,40}
    distances[5] = [8]int{60,75,103,12,129,0,15,99}
    distances[6] = [8]int{71,82,49,18,53,15,0,70}
    distances[7] = [8]int{67,54,97,91,40,99,70,0}	
    
    // create a set to permute. use the integers 1..n.
    s := make([]int, n)
    for i := range s {
        s[i] = i + 1
    }
    min := 1000
    
    // permute them, calling a function for each permutation.
    permute(s, func(p []int) {
        sum := 0
	for i := 0; i < 7; i++ {
	    sum += distances[p[i]-1][p[i+1]-1]
        }
	if (sum < min) {
	    fmt.Println(p) 
	    fmt.Println(sum)
	    min = sum
	} 
    })
    fmt.Println(max)
}
 
// NOT MINE
// permute function.  takes a set to permute and a function
// to call for each generated permutation.
func permute(s []int, emit func([]int)) {
    if len(s) == 0 {
        emit(s)
        return
    }
    // Steinhaus, implemented with a recursive closure.
    // arg is number of positions left to permute.
    // pass in len(s) to start generation.
    // on each call, weave element at pp through the elements 0..np-2,
    // then restore array to the way it was.
    var rc func(int)
    rc = func(np int) {
        if np == 1 {
            emit(s)
            return
        }
        np1 := np - 1
        pp := len(s) - np1
        // weave
        rc(np1)
        for i := pp; i > 0; i-- {
            s[i], s[i-1] = s[i-1], s[i]
            rc(np1)
        }
        // restore
        w := s[0]
        copy(s, s[1:pp+1])
        s[pp] = w
    }
    rc(len(s))
}
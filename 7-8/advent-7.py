import hashlib

def main():
    base = 'ckczppom'
    for i in range(10000000):
        input = base+str(i)
        m = hashlib.md5()
        m.update(input.encode('utf-8'))
        hex = m.hexdigest()
        if (hex[0:5] == '00000'):
            print(i)
            break
    #end loop
#end main

if __name__ == "__main__":
    main()
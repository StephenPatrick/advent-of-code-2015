require 'digest'

def main()
    base = 'ckczppom'
    (0..10000000).each do |i|
        md5 = Digest::MD5.new
        md5.update base + i.to_s
        hex = md5.hexdigest
        if (hex.slice(0,6) == '000000')
            puts(i)
            break
        end
    end
end

main()
function leftRotate(x,y)
    return (x << y) | (x >> (32-y))
end

function stringToBinary(s)
    return string.byte(s:sub(1))*2^24 + string.byte(s:sub(2))*2^16 + string.byte(s:sub(3))*2^8 + string.byte(s:sub(4))
end

function main() 
    
    shifts = {7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,  7, 12, 17, 22,
                    5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,  5,  9, 14, 20,
                    4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,  4, 11, 16, 23,
                    6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21,  6, 10, 15, 21}

    constants = {}
    two32nd = math.pow(2,32)
    for i = 0,63,1 do
        table.insert(constants, math.floor(two32nd * math.abs(math.sin(i+1))))
    end
    
    a0 = 0x67452301   
    b0 = 0xefcdab89   
    c0 = 0x98badcfe   
    d0 = 0x10325476
        
    message = "abcdef609043"
   -- The number of bits in the original message.
    messageBitSize_init = string.len(message)*8
    -- The number of bits the previous value takes up
    messageBitSizeBitLength = math.floor(math.log(messageBitSize_init) / math.log(2))
    while (messageBitSizeBitLength % 8 ~= 0) do
        messageBitSizeBitLength = messageBitSizeBitLength + 1
    end
    
    -- 128 is 10000000 in binary, and we need to append a 1.
    message = message .. string.char(128)
    messageBitSize = messageBitSize_init + 8;    
    
   while (messageBitSize % 512 ~= 512-messageBitSizeBitLength) do
        message = message .. "\0"
        messageBitSize = messageBitSize + 8
    end
    
    while messageBitSize_init > 256 do
        remainder = messageBitSize_init % 256
        messageBitSize_init = messageBitSize_init - 256
        message = message .. char(remainder)
    end
    
    message = message .. string.char(messageBitSize_init)
    
    finalMessageSize = string.len(message)*8 
    print(finalMessageSize)
    while (finalMessageSize > 0) do
        finalMessageSize = finalMessageSize - 512
        --messagepiece = ''
        --if (finalMessageSize ~= 0) then
        --    messagepiece = "technically possible, but shouldn't happen"
        --else 
        messagepiece = message
        --end
        
        substrings = {}
        
        for i = 0,15,1 do
            table.insert(substrings, string.sub(messagepiece,(i*4)+1,(i*4)+4))
        end
        for key,value in pairs(substrings) do
            print(key,value)
        end
        A = a0
        B = b0
        C = c0
        D = d0
        for i=1,64,1 do
            F = 0
            G = 0
            if (i < 16) then
                F = (B & C) | ((~B) & D)
                G = i
            elseif (i < 32) then
                F = (D & B) | ((~D) & C)
                G = (5*i) % 16
            elseif (i < 48) then
                F = B ~ C ~ D
                G = (3*i) % 16
            else 
                F = C ~ (B | (~D))
                G = (7*i) % 16
            end
            oldD = D
            D = C
            C = B
            G = G + 1 -- Indexing by 1 fix
            B = leftRotate(A+F+constants[i]+stringToBinary(substrings[G]),shifts[i])
            A = oldD
        end
        a0 = a0 + A
        b0 = b0 + B
        c0 = c0 + C
        d0 = d0 + D
    end
    
    print(a0)
    print(b0)
    print(c0)
    print(d0)
end

main()

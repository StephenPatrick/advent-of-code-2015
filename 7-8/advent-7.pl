use warnings;
use strict;
use Digest::MD5 qw(md5_hex);

sub main{
    my $base = 'ckczppom';
    for (my $i = 0;;$i++){
        my $hex = md5_hex($base.$i);
        if (substr($hex,0,5) eq '00000'){
            print $i;
            last;
        }
    }
}
main();
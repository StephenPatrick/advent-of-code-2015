use warnings;
use strict;
use Digest::MD5 qw(md5_hex);

sub main{
    my $base = 'ckczppom';
    for (my $i = 0;;$i++){
        my $hex = md5_hex($base.$i);
        if (substr($hex,0,6) eq '000000'){
            print $i;
            last;
        }
    }
}
main();
def ribbon(l,w,h)
  return  l * w  * h + 2*(l+w+h -[l,w,h].max)
end

def main()
    sum = 0
    dimensions = open('advent-3-input.txt', 'r').read
    dimensions.each_line do |line|
        (l,w,h) = line.split('x')
        sum += ribbon(l.to_i,w.to_i,h.to_i)
    end
    puts sum
end

main()
function wrapping_paper(l,w,h)
    sum = 0
    side1 = l * w * 2
    side2 = w * h * 2
    side3 = h * l * 2
    sum = sum + side1 + side2 + side3
    return sum + (math.min(side1,side2,side3) /2)
end

function main()
    sum = 0
    dimensions = assert(io.open('advent-3-input.txt', 'r'))
    io.input(dimensions)
    line = io.read("*line")
    while line ~= nil do
        --Lua does not have a string split function.
        t = string.gmatch(line, "%d+")
        sum = sum + wrapping_paper(t(),t(),t())
        line = io.read("*line")
    end
    print(sum)
end

main()
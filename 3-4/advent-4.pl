use warnings;
use strict;
use List::Util qw[min];

sub ribbon {
  my @dimensions = @_;
  my $sum = $dimensions[0] * $dimensions[1]  * $dimensions[2];
  # This was way too hard to figure out the formatting for
  @dimensions = sort({$a <=> $b} @dimensions);
  return $sum + 2*($dimensions[0]+$dimensions[1]);
}

sub main {
  my $sum = 0;
  open my $dimensions, '<', 'advent-3-input.txt';
  while (my $row = <$dimensions>) {
    my @split = split('x',$row);
    $sum += ribbon(@split);
  }
  print($sum);
}

main();
function ribbon(l,w,h)
    return (l * w * h) + 2*(l+w+h - math.max(l,w,h))
end

function main()
    sum = 0
    dimensions = assert(io.open('advent-3-input.txt', 'r'))
    io.input(dimensions)
    line = io.read("*line")
    while line ~= nil do
        --Lua does not have a string split function.
        t = string.gmatch(line, "%d+")
        sum = sum + ribbon(tonumber(t()),tonumber(t()),tonumber(t()))
        line = io.read("*line")
    end
    print(sum)
end

main()
<?php
    function ribbon($l,$w,$h) {
        return  ($l * $w  * $h) + 2*($l+$w+$h - max($l,$w,$h));
    }
    function main() {
        $sum = 0;
        $file = fopen("advent-3-input.txt","r");
        while (($line = fgets($file)) !== false) {
            $input = explode('x',$line);
            $sum += ribbon(intval($input[0]),intval($input[1]),intval($input[2]));
        }
        print($sum);
    }

    main();
?>
<?php
    function wrapping_paper($l,$w,$h) {
        $side1 = 2 * $l * $w;
        $side2 = 2 * $w * $h;
        $side3 = 2 * $h * $l;
        $sum = $side1 + $side2 + $side3;
        return $sum + min($side1, $side2, $side3) / 2;
    }
    
    function main() {
        $sum = 0;
        $file = fopen("advent-3-input.txt","r");
        while (($line = fgets($file)) !== false) {
            $input = explode('x',$line);
            $sum += wrapping_paper($input[0],$input[1],$input[2]);
        }
        print($sum);
    }

    main();
?>
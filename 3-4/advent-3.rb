def wrapping_paper(l,w,h)
    side1 = 2 * l * w
    side2 = 2 * w * h
    side3 = 2 * h * l
    sum = side1 + side2 + side3
    return sum + [side1, side2, side3].min / 2
end

def main()
    sum = 0
    dimensions = open('advent-3-input.txt', 'r').read
    dimensions.each_line do |line|
        (l,w,h) = line.split('x')
        sum += wrapping_paper(l.to_i,w.to_i,h.to_i)
    end
    puts sum
end

main()
def wrapping_paper(l,w,h):
    sum = 0;
    side1 = 2 * l * w;
    side2 = 2 * w * h;
    side3 = 2 * h * l;
    sum += side1;
    sum += side2;
    sum += side3;
    sum += min(side1, side2, side3) / 2;
    return sum;

def main():
    sum = 0;
    dimensions = open('advent-3-input.txt', 'r');
    lines = dimensions.readlines()
    for line in lines:
        (l,w,h) = line.split('x');
        sum += wrapping_paper(int(l),int(w),int(h));
    print(sum);

if __name__ == "__main__":
    main(); 
def ribbon(l,w,h):
    return  l * w  * h + 2*(l+w+h - max(l,w,h));

def main():
    sum = 0;
    dimensions = open('advent-3-input.txt', 'r');
    lines = dimensions.readlines()
    for line in lines:
        (l,w,h) = line.split('x');
        sum += ribbon(int(l),int(w),int(h));
    print(sum);
    
if __name__ == "__main__":
    main();
use warnings;
use strict;
use List::Util qw[min];

sub wrapping_paper {
  my ($l, $w, $h) = @_;
  my $sum = 0;
  my $side1 = 2 * $l * $w;
  my $side2 = 2 * $w * $h;
  my $side3 = 2 * $h * $l;
  $sum += $side1;
  $sum += $side2;
  $sum += $side3;
  $sum += min($side1, $side2, $side3) / 2;
  return $sum;
}

sub main {
  my $sum = 0;
  open my $dimensions, '<', 'advent-3-input.txt';
  while (my $row = <$dimensions>) {
    $sum += wrapping_paper(split('x',$row));
  }
  print($sum);
}

main();
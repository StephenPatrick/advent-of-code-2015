import java.util.regex.Pattern;
import java.util.regex.Matcher;

class Advent21 {

    public static void main(String[] args){

        char[] old_password = {'c','q','j','x','j','n','d','s'};
        Pattern bad_letters = Pattern.compile(".*[i|l|o].*");
        Pattern doubles = Pattern.compile(".*(.)\\1.*(.)\\2.*");
        boolean password_found = false;
        while (!password_found) {
            char[] new_password = inc_string(old_password);
            String str_password = new String(new_password);
            Matcher bad_letters_matcher = bad_letters.matcher(str_password);
            Matcher doubles_matcher = doubles.matcher(str_password);
            if ((!bad_letters_matcher.matches()) && doubles_matcher.matches()) {

                for (int i = 0; i < new_password.length-2; i++) {
                    if ((int)new_password[i] == (int)new_password[i+1]-1 &&
                        (int)new_password[i] == (int)new_password[i+2]-2) {
                            System.out.println(new_password);
                            password_found = true;
                    }
                }
            }
            old_password = new_password;
        }

    }

    public static char[] inc_string(char[] password){

        for (int i = password.length-1; i > -1; i--) {
            int new_val = ((((int) password[i]) + 1) - 97) % 26;
            password[i] = (char) (new_val + 97);
            // So long as we didn't loop to a, don't increment
            // the letter to the left.
            if (new_val != 0) {
                break;
            }
        }

        return password;

    }

}

package main

import "bufio"
import "os"
import "fmt"
import "regexp"
import "strconv"

func isOpcode(s string) bool {
    return (s == "NOT" || s == "AND" ||
                s == "OR" || s == "LSHIFT" ||
                s == "RSHIFT") 
}

func isInt(s string) bool {
    comp := regexp.MustCompile(`^[\d\-]*$`)
    return comp.MatchString(s)
}

func g_atoi(s string) int{
    i, _ := strconv.Atoi(s)
    return i
}

// Didn't write this
func singleContains(s []string, e string) int {
    for i, a := range s {
        if a == e {
            return i
        }
    }
    return -1
}

func main() {
    
    file, _ := os.Open("advent-15-input.txt") // For read access.
    
    scanner := bufio.NewScanner(file)

    codeChars := 0
    realChars := 0
    
    // Process input
    for scanner.Scan() {
        line := scanner.Text()
        i := 0
        for i < len(line) {
            codeChars++
            realChars++
            c := line[i]
            if (c == '\\' && 
                (line[i+1] == '"'|| line[i+1] == '\\')) {
                i++
                codeChars++
            } else if (c=='\\' && line[i+1] == 'x') {
                i += 3
                codeChars+= 3
            }
            i++
        }
        // Quotation marks
        realChars -= 2
    }
    fmt.Println(codeChars, realChars, codeChars - realChars)
}
require 'set'

def main
    x = 0
    y = 0
    robox = 0
    roboy = 0
    santa = 0
    poslist = Set.new [[0,0]]
    directions = File.open("advent-5-input.txt", "r")
    directions.each_char do |input|
        if santa % 2 == 0 
            if input == '^'
                y += 1
            elsif input == 'v'
                y -= 1 
            elsif input == '<'
                x -=1
            elsif input == '>'
                x += 1
            end
            poslist.add([x,y])
        else
            if input == '^'
                roboy += 1
            elsif input == 'v'
                roboy -= 1 
            elsif input == '<'
                robox -=1
            elsif input == '>'
                robox += 1
            end
            poslist.add([robox,roboy])
        end
        santa += 1
    end
    puts poslist.length
end

main()
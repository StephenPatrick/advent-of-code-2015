require 'set'

def main
    x = 0
    y = 0
    poslist = Set.new [[0,0]]
    directions = File.open("advent-5-input.txt", "r")
    directions.each_char do |input|
        if input == '^'
            y += 1
        end
        if input == 'v'
            y -= 1
        end 
        if input == '<'
            x -=1
        end
        if input == '>'
            x += 1
        end
        poslist.add([x,y])
    end
    puts poslist.length
end

main()
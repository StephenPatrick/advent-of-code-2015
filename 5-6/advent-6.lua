function main()
    x = 0
    y = 0
    robox = 0
    roboy = 0
    santa = 0
    hash = {}
    hash["0,0"] = true
    length = 1
    directions = assert(io.open('advent-5-input.txt', 'r'))
    io.input(directions)
    while true do
        input = io.read(1)
        if input == nil then
            break
        elseif santa % 2 == 0 then
            if input == '^' then
                y = y + 1
            elseif input == 'v' then
                y = y - 1
            elseif input == '<' then
                x = x - 1
            elseif input == '>' then
                x = x + 1
            end
            position = x..','..y
            if (not hash[position]) then
                hash[position] = true
                length = length + 1
            end
        else
            if input == '^' then
                roboy = roboy + 1
            elseif input == 'v' then
                roboy = roboy - 1
            elseif input == '<' then
                robox = robox - 1
            elseif input == '>' then
                robox = robox + 1
            end
            position = robox..','..roboy
            if (not hash[position]) then
                hash[position] = true
                length = length + 1
            end
        end
        santa = santa + 1
    end
    print(length)
end

main()
def main():
    x = 0
    y = 0
    poslist = set()
    poslist.add((0,0))
    directions = open("advent-5-input.txt", "r")
    while True:
        input = directions.read(1)
        if not input:
            break
        if input == '^':
            y += 1
        elif input == 'v':
            y -= 1
        elif input == '<':
            x -=1
        elif input == '>':
            x += 1
        poslist.add((x,y))
    print(len(poslist))

if __name__ == "__main__":
    main()
<?php
    function main() {
        $x = 0;
        $y = 0;
        $santa = 0;
        $robox = 0;
        $roboy = 0;
        $set =["0,0" => 1];
        $file = fopen("advent-5-input.txt","r");
        while (($input = fgetc($file)) !== false) {
            if ($santa % 2 == 0) {
                if ($input == '^'){
                    $y++;
                }elseif ($input == 'v'){
                    $y--;
                }elseif ($input == '<'){
                    $x--;
                }elseif ($input == '>'){
                    $x++;
                }
                $set["$x,$y"]=1;
            } else {
                if ($input == '^'){
                    $roboy++;
                }elseif ($input == 'v'){
                    $roboy--;
                }elseif ($input == '<'){
                    $robox--;
                }elseif ($input == '>'){
                    $robox++;
                }
                $set["$robox,$roboy"]=1;
            }
            $santa++;
        }
        print(count($set));
    }

    main()
?>
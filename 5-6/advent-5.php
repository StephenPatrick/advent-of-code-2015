<?php
    function main() {
        $x = 0;
        $y = 0;
        $set =["0,0" => 1];
        $file = fopen("advent-5-input.txt","r");
        while (($input = fgetc($file)) !== false) {
            if ($input == '^'){
                $y++;
            }elseif ($input == 'v'){
                $y--;
            }elseif ($input == '<'){
                $x--;
            }elseif ($input == '>'){
                $x++;
            }
            $set["$x,$y"]=1;
        }
        print(count($set));
    }

    main()
?>
use warnings;
use strict;
use v5.14;

sub main{
    my $x = 0;
    my $y = 0;
    my $santa = 0;
    my $robox = 0;
    my $roboy = 0;
    my %set =();
    $set{"(0,0)"}=1; #Sets exist in CPAN, but strings are a good native alternative.
    open my $directions, '<', 'advent-5-input.txt';
    while ((my $n = read ($directions, my $input, 1)) != 0) {
        if ($santa % 2 == 0) {
            if ($input eq '^'){
                $y++;
            }elsif ($input eq 'v'){
                $y--;
            }elsif ($input eq '<'){
                $x--;
            }elsif ($input eq '>'){
                $x++;
            }
            $set{"($x,$y)"}=1;
        } else {
            if ($input eq '^'){
                $roboy++;
            }elsif ($input eq 'v'){
                $roboy--;
            }elsif ($input eq '<'){
                $robox--;
            }elsif ($input eq '>'){
                $robox++;
            }
            $set{"($robox,$roboy)"}=1;
        }
        $santa++;
    }
    say (scalar keys %set); #To count a hash table, get the count of keys.
                                            #Scalar %set gives the fraction of buckets filled in the table.
}

main()
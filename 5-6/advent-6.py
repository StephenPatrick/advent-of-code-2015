def main():
    x = 0
    y = 0
    robox = 0
    roboy = 0
    santa = 0
    poslist = set()
    poslist.add((0,0)) #Python requires tuples instead of lists here.
                                    #Lists are unhashable and can't be members of sets.
    directions = open("advent-5-input.txt", "r")
    while True:
        input = directions.read(1)
        if santa % 2 == 0:
            if not input:
                break
            if input == '^':
                y += 1
            elif input == 'v':
                y -= 1
            elif input == '<':
                x -=1
            elif input == '>':
                x += 1
            poslist.add((x,y))
        else:
            if input == '^':
                roboy += 1
            elif input == 'v':
                roboy -= 1 
            elif input == '<':
                robox -=1
            elif input == '>':
                robox += 1
            poslist.add((robox,roboy))
        santa += 1
    print(len(poslist))

if __name__ == "__main__":
    main()
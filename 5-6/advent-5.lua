function main()
    x = 0
    y = 0
    hash = {}
    hash["0,0"] = true
    length = 1
    directions = assert(io.open('advent-5-input.txt', 'r'))
    io.input(directions)
    while true do
        input = io.read(1)
        if input == nil then
            break
        elseif input == '^' then
            y = y + 1
        elseif input == 'v' then
            y = y - 1
        elseif input == '<' then
            x = x - 1
        elseif input == '>' then
            x = x + 1
        end
        position = x..','..y
        if (not hash[position]) then
            hash[position] = true
            length = length + 1
        end
    end
    print(length)
end

main()
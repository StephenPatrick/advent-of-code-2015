function main()
    sum = 0
    strings = assert(io.open('advent-9-input.txt', 'r'))
    io.input(strings)
    line = io.read("*line")
    while line ~= nil do
        --Lua does not have smart regex matching.
        --There are regular expression extensions, 
        --But as no other language needed something non-native,
        --We're doing this the hard way
        prevchar = nil
        prevprevchar = nil
        doubles = false
        skiprepeat = false
        evenpairs = {}
        i = 0
        for c in line:gmatch"." do
            if (prevprevchar == c) then
                skiprepeat = true
            end
            if (prevchar) then
                evenpairs[i] = prevchar..c
            end
            prevprevchar = prevchar
            prevchar = c
            i = i + 1
        end
        for k,v in pairs(evenpairs) do
            for k2,v2 in pairs(evenpairs) do
                if (k2 > k+1) then
                    if (v == v2) then
                        doubles = true
                    end
                end
            end
        end
        if (skiprepeat and doubles) then
            sum = sum + 1
        end
        line = io.read("*line")
    end
    print(sum)
end

main()
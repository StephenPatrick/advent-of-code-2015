use warnings;
use strict;

sub main{
    my $sum = 0;
    open my $file, '<', 'advent-9-input.txt';
    while (my $line = <$file>) {
        if ($line =~ /^.*(.)(.)\1.*$/) {
            if ($line =~ /^.*(..).*\1.*$/) {
                $sum++;
            }
        }
    }
    print($sum);
}

main();
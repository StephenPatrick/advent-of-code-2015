use warnings;
use strict;

sub main{
    my $sum = 0;
    open my $file, '<', 'advent-9-input.txt';
    while (my $line = <$file>) {
        if ($line =~ /^(.*[aeiou].*){3}$/) {
            if ($line =~ /^.*(.)\1.*$/) {
                if (!($line =~ /^.*(pq|cd|ab|xy).*$/)) {
                    $sum++;
                }
            }
        }
    }
    print($sum);
}

main();
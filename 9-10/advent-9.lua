function main()
    sum = 0
    strings = assert(io.open('advent-9-input.txt', 'r'))
    io.input(strings)
    line = io.read("*line")
    while line ~= nil do
        --Lua does not have smart regex matching.
        --There are regular expression extensions, 
        --But as no other language needed something non-native,
        --We're doing this the hard way
        prevchar = nil
        vowels = 0
        doubles = false
        fail = false
        for c in line:gmatch"." do
            if (c == 'a' or c == "e" or c == "i" or c == "o" or c == "u") then
                vowels = vowels + 1
            end
            if (c == prevchar) then
                doubles = true
            end
            if ((c == "b" and prevchar == "a") or (c == "d" and prevchar == "c") 
                or (c == "y" and prevchar == "x") or (c == "q" and prevchar == "p")) then
                fail = true
                break
            end
            prevchar = c
        end
        if (not fail and doubles and vowels > 2) then
            sum = sum + 1
        end
        line = io.read("*line")
    end
    print(sum)
end

main()
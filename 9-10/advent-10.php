<?php
    $sum = 0;
    $file = fopen("advent-9-input.txt","r");
    while (($line = fgets($file)) !== false) {
        if (preg_match('/^.*(.)(.)\1.*$/',$line)){
            if (preg_match('/^.*(..).*\1.*$/',$line)) {
                $sum++;           
            }
        }
    }
    print($sum);
?>
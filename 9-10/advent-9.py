import re

def main():
    sum = 0;
    strings = open('advent-9-input.txt', 'r');
    lines = strings.readlines()
    for line in lines:
        if (re.search("^(.*[aeiou].*){3}$", line)):
            if (re.search("^.*(.)\\1.*$", line)):
                if  not (re.search('^.*(pq|cd|ab|xy).*$', line)):
                    sum += 1;
    print(sum);

if __name__ == "__main__":
    main(); 
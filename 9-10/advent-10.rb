def main()
    sum = 0
    strings = open('advent-9-input.txt', 'r').read
    strings.each_line do |line|
        if (/^.*(.)(.)\1.*$/ =~ line)
            if (/^.*(..).*\1.*$/ =~ line)
                sum = sum + 1
            end
        end
    end
    puts sum
end

main()